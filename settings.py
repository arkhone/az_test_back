# URL's
STAGING_URL = "https://api2.staging.azalia-now.ru/"
TEST1_URL = "https://api2.test1.azalia-now.ru/"

# PATH's
AUTH_PATH = "v1/api/auth/employee/login"
# ATTRS/LABELS
LABEL_ID_PATH = "v1/api/attrs/labels/32?include%5Battr_values%5D=true"

# JSON's

ADMIN = {"phone": "+7 (988) 111-11-04", "password": "0104"}

# HEADERS

AUTH_HEADER = {
  'accept': 'application/json',
  'request-source': 'mobile',
  'Content-Type': 'application/json'
}
