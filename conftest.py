import allure
import pytest
import settings as s
from utils.client import ApiClient


@pytest.fixture
def staging():
    return ApiClient(base_address=s.STAGING_URL)


@pytest.fixture
def test1():
    return ApiClient(base_address=s.TEST1_URL)


@pytest.fixture
def get_access_token(staging):
    with allure.step(f'Try to get accessToken'):
        response = staging.post(path=s.AUTH_PATH, json=s.ADMIN, headers=s.AUTH_HEADER)
        json = response.json()
        access_token = json["data"].get("accessToken")
        headers = {
            'accept': 'application/json',
            'Authorization': f"{access_token}"}
        return headers



@pytest.fixture
def get_refresh_token(staging):
    with allure.step(f'Try to get refreshToken'):
        response = staging.post(path=s.AUTH_PATH, json=s.ADMIN, headers=s.AUTH_HEADER)
        json = response.json()
        refresh_token = json["data"].get("refreshToken")
        return f"{refresh_token}"

