from typing import List

from pydantic import BaseModel, Field


class LabelBody(BaseModel):
    id: int
    name: str
    slug: str
    is_active: bool
    created_at: str
    updated_at: str
    attr_values: List[dict]


class MainLabel(BaseModel):
    status: str
    error: None | str
    data: LabelBody

