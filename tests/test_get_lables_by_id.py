import settings as s
from assertions.assertion_base import assert_status_code, assert_schema
from models.labels.get_label_model import MainLabel


def test_smoke(get_access_token, staging):
    headers = get_access_token
    response = staging.get(path=s.LABEL_ID_PATH, headers=headers)
    assert_status_code(response, 200)
    assert_schema(response, MainLabel)
